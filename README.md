Sitio web estático diseñado con Adobe Xd y desarrollado con HTML, Sass y JavaScrip para Craver SA, una empresa local dedicada a importar muebles desde Brasil. El sitio esta totalmente adaptado para dispositivos moviles y utiliza un app propia de mailing desarrollada con C# para el formulario de contacto. 


Static website designed with Adobe Xd and developed with HTML, Sass and JavaScrip for Craver SA, a local company dedicated to importing furniture from Brazil. The site is fully adapted for mobile devices and uses its own mailing app developed with C# for the contact form.
