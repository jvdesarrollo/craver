const menu = document.getElementById("menu");
const menuInit = document.getElementById("menuInit");
const menuCard = document.querySelector(".menuCard");
const menuCardInit = document.querySelector(".menuCardInit");
const backdrop = document.querySelector(".backdrop");
const close = document.getElementById("close");
const closeInit = document.getElementById("closeInit");

const toggleBackdrop = () => {
  backdrop.classList.toggle("backdropVisible");
};
const showMenu = () => {
  menuCard.classList.add("visible");
  menuCardInit.classList.add("visible");
  toggleBackdrop();
};
const hideMenu = () => {
  menuCard.classList.remove("visible");
  menuCardInit.classList.remove("visible");

  toggleBackdrop();
};
menu.addEventListener("click", showMenu);
menuInit.addEventListener("click", showMenu);
close.addEventListener("click", hideMenu);
closeInit.addEventListener("click", hideMenu);

const button = document.getElementById("prod");
button.addEventListener("click", (event) => {
  event.preventDefault();
  window.open("productos.html", "_self");
});

const scroll =
  window.requestAnimationFrame ||
  function (callback) {
    window.setTimeout(callback, 1000 / 60);
  };

const elementsToShow = document.querySelectorAll(".show");
const elementsToShowP = document.querySelectorAll(".show-p");

function loop() {
  elementsToShow.forEach(function (element) {
    if (isElementInViewport(element)) {
      element.classList.add("is-visible");
    } 
  });
  elementsToShowP.forEach(function (element) {
    if (isElementInViewport(element)) {
      element.classList.add("is-visible");
    } 
  });

  scroll(loop);
}

loop();

function isElementInViewport(el) {
  // special bonus for those using jQuery
  if (typeof jQuery === "function" && el instanceof jQuery) {
    el = el[0];
  }
  var rect = el.getBoundingClientRect();
  return (
    (rect.top <= 0 && rect.bottom >= 0) ||
    (rect.bottom >=
      (window.innerHeight || document.documentElement.clientHeight) &&
      rect.top <=
        (window.innerHeight || document.documentElement.clientHeight)) ||
    (rect.top >= 0 &&
      rect.bottom <=
        (window.innerHeight || document.documentElement.clientHeight))
  );
}

let prevScrollpos = window.pageYOffset;
window.onscroll = function () {
  let currentScrollPos = window.pageYOffset;

  if (prevScrollpos) {
    document.getElementById("scroll").style.top = "0";
    document.getElementById("navScroll").style.opacity = "0";
    document.getElementById("scroll-mobile").style.top = "0";
    document.getElementById("navScroll-mobile").style.opacity = "0";
    document.getElementById("logo-scroll").style.opacity = "0";
  }
  if (currentScrollPos === 0) {
    document.getElementById("scroll").style.top = "-130px";
    document.getElementById("navScroll").style.opacity = "1";
    document.getElementById("scroll-mobile").style.top = "-130px";
    document.getElementById("navScroll-mobile").style.opacity = "1";
    document.getElementById("logo-scroll").style.opacity = "1";
  }

  prevScrollpos = currentScrollPos;
};
