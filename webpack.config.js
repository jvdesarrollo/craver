const autoprefixer = require("autoprefixer");
module.exports =[

  {
    entry: ["./index.scss", "./index.js"],
    output: {
      // This is necessary for webpack to compile
      // But we never use style-bundle.js
      path: __dirname,
      publicPath: "/",
      filename: "bundle.js"
    },

    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [
            {
              loader: "file-loader",
              options: {
                name: "bundle.css"
              }
            },
            { loader: "extract-loader" },
            { loader: "css-loader" },
            {
              loader: "postcss-loader",
              options: {
                plugins: () => [autoprefixer()]
              }
            },
            {
              loader: "sass-loader",
              options: {
                sassOptions: {
                  includePaths: ["./node_modules"]
                }
              }
            }
          ]
        },
        {
          test: /\.js$/,
          loader: "babel-loader",
          query: {
            presets: ["@babel/preset-env"]
          }
        }
      ]
    }
  },

  {
    entry: ["./empresa.scss", "./empresa.js"],
    output: {
      // This is necessary for webpack to compile
      // But we never use style-bundle.js
      path: __dirname,
      publicPath: "/",
      filename: "bundle-empresa.js"
    },

    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [
            {
              loader: "file-loader",
              options: {
                name: "bundle-empresa.css"
              }
            },
            { loader: "extract-loader" },
            { loader: "css-loader" },
            {
              loader: "postcss-loader",
              options: {
                plugins: () => [autoprefixer()]
              }
            },
            {
              loader: "sass-loader",
              options: {
                sassOptions: {
                  includePaths: ["./node_modules"]
                }
              }
            }
          ]
        },
        {
          test: /\.js$/,
          loader: "babel-loader",
          query: {
            presets: ["@babel/preset-env"]
          }
        }
      ]
    }
  },

  {
    entry: ["./productos.scss", "./productos.js"],
    output: {
      // This is necessary for webpack to compile
      // But we never use style-bundle.js
      path: __dirname,
      publicPath: "/",
      filename: "bundle-productos.js"
    },

    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [
            {
              loader: "file-loader",
              options: {
                name: "bundle-productos.css"
              }
            },
            { loader: "extract-loader" },
            { loader: "css-loader" },
            {
              loader: "postcss-loader",
              options: {
                plugins: () => [autoprefixer()]
              }
            },
            {
              loader: "sass-loader",
              options: {
                sassOptions: {
                  includePaths: ["./node_modules"]
                }
              }
            }
          ]
        },
        {
          test: /\.js$/,
          loader: "babel-loader",
          query: {
            presets: ["@babel/preset-env"]
          }
        }
      ]
    }
  },
  
  {
    entry: ["./contacto.scss", "./contacto.js"],
    output: {
      // This is necessary for webpack to compile
      // But we never use style-bundle.js
      path: __dirname,
      publicPath: "/",
      filename: "bundle-contacto.js"
    },

    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [
            {
              loader: "file-loader",
              options: {
                name: "bundle-contacto.css"
              }
            },
            { loader: "extract-loader" },
            { loader: "css-loader" },
            {
              loader: "postcss-loader",
              options: {
                plugins: () => [autoprefixer()]
              }
            },
            {
              loader: "sass-loader",
              options: {
                sassOptions: {
                  includePaths: ["./node_modules"]
                }
              }
            }
          ]
        },
        {
          test: /\.js$/,
          loader: "babel-loader",
          query: {
            presets: ["@babel/preset-env"]
          }
        }
      ]
    }
  },
  
];

